from sage.graphs.graph_decompositions.modular_decomposition import *
from sage.graphs.connectivity import *
import sminer.sminer as sm

def mine_model(log, epsilon):
    dfg, self_loop, short_loop, concurrency, _pre_short_loop = sm.compute_behavior_relations(log, epsilon)

    for a in self_loop:
        dfg.pop((a,a), None)
    for pair in concurrency:
        dfg.pop(pair, None)

    fdfg = sm.filter_dfg(dfg)
    miner = GatewayMiner(fdfg, concurrency)
    processor = RPSTProcessor(miner.model, miner.counter)
    return processor.model

class GatewayMiner:
    def __init__(self, dfg, concurrency, counter = 0):
        self.counter = counter
        self.model = dfg.copy()
        self.__add_split_gateways(concurrency)
        self.__add_join_gateways()
    
    def __add_split_gateways(self, concurrency):
        visited = set()
        stack = []
        stack.append('__INITIAL__')
        while len(stack) > 0:
            current = stack.pop()
            visited.add(current)
            successors = list(map(lambda pair: pair[1], filter(lambda pair: pair[0] == current, self.model)))

            # is CURRENT a split node?
            if len(successors) > 1:
                mdt_graph = Graph()
                mdt_graph.add_vertices(successors)        
                mdt_graph.add_edges([e for e in concurrency if e[0] in successors and e[1] in successors])        
                mdt = modular_decomposition(mdt_graph)

                gw, edges = self.__add_gateways(mdt)

                for succ in successors:
                    self.model.remove((current, succ))
                for edge in edges:
                    self.model.add(edge)
                self.model.add((current, gw))

            for succ in successors:
                if succ not in visited and succ not in stack:
                    stack.append(succ)
                    
    def __add_join_gateways(self):
        visited = set()
        stack = []
        stack.append('__FINAL__')
        while len(stack) > 0:
            current = stack.pop()
            visited.add(current)
            predecessors = list(map(lambda pair: pair[0], filter(lambda pair: pair[1] == current, self.model)))

            # is CURRENT a join node?
            if len(predecessors) > 1:
                gw = "OR_" + str(self.counter)
                self.counter += 1

                for pred in predecessors:
                    self.model.remove((pred, current))
                    self.model.add((pred, gw))

                self.model.add((gw, current))

            for pred in predecessors:
                if pred not in visited and pred not in stack:
                    stack.append(pred)
                    
    def __add_gateways(self, mdt):
        self.edges = []
        gw = self.__traverse(mdt)
        return gw, self.edges
    
    def __traverse(self, current):
        if current.node_type is NodeType.NORMAL:
            return current.children[0]
        else:
            children = []
            for child in current.children:
                children.append(self.__traverse(child))
            gw = ("AND_" if current.node_type is NodeType.SERIES else "XOR_") + str(self.counter)
            self.counter += 1
            for child in children:
                self.edges.append((gw, child))
            return gw

class RPSTProcessor:
    def __init__(self, model, counter):
        self.model = model.copy()
        self.vedgesmap = {}
        self.counter = counter
        
        graph = Graph()
        graph.add_edges(model)
        graph.add_edges([('__INITIAL__', '__FINAL__')])
        tree = spqr_tree(graph)

        root = None
        for comp in tree:
            for n1, n2, vedge in comp[1].edge_iterator():
                if vedge is None: continue
                if vedge in self.vedgesmap:
                    self.vedgesmap[vedge].append(comp)
                else:
                    self.vedgesmap[vedge] = [comp]
            if '__INITIAL__' in comp[1].vertices():
                root = comp
        
        self.visited = set()
        self.__traverse(root)
    
    def __refactor_gateway(self, current, gw, vertices):
        predecessors = set([src for src, tgt in self.model if tgt == current])
        
        for predecessor in predecessors.intersection(vertices):
            self.model.remove((predecessor, current))
            self.model.add((predecessor, gw))

        if predecessors.issubset(vertices):
            successor = [tgt for src, tgt in self.model if src == current][0]
            self.model.remove((current, successor))
            self.model.add((gw, successor))
        else:
            self.model.add((gw, current))

    def __traverse(self, current):
        node_type, fragment = current
        self.visited.add(current)
        
        vertices = set(fragment.vertices())
        
        for src,tgt,vedge in fragment.edge_iterator():
            if vedge is None: continue
            child = self.vedgesmap[vedge][0] if self.vedgesmap[vedge][0] != current else self.vedgesmap[vedge][1]
            if child not in self.visited:
                vertices |= self.__traverse(child)
        
        if node_type == 'P':
            entry_node, exit_node = fragment.vertices()
            
            if entry_node.startswith("OR"): 
                entry_node, exit_node = exit_node, entry_node

            gw = ("XOR_" if entry_node.startswith("XOR") else "AND_") + str(self.counter)
            self.counter += 1
            vertices.add(gw)
            self.__refactor_gateway(exit_node, gw, vertices)
            
        elif node_type == 'R':
            gwtype = set([v[:4] for v in fragment.vertices() if not v.startswith('OR')])
            if len(gwtype) == 1: # HOMOGENEOUS
                gwtype = gwtype.pop()
                for vertex in fragment.vertices():
                    if not vertex.startswith('OR'): continue
                    gw = gwtype + str(self.counter)
                    self.counter += 1
                    vertices.add(gw)
                    self.__refactor_gateway(vertex, gw, vertices)        
        return vertices
    def get_model(self):
        return self.model    