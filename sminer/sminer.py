import networkx as nx
from pm4pybpmn.objects.bpmn.importer.bpmn_diagram_rep import *

def compute_behavior_relations(log, epsilon):
    dfg = {}
    pre_short_loop = {}
    self_loop = {}

    # We use a single log traversal to determine Directly-follows, Self-loop and Pre-short loop relations
    for trace in log:
        pprevious = None
        previous = '__INITIAL__'
        for event in trace:
            if event['lifecycle:transition'] != 'complete':
                continue
            current = event['concept:name']

            dfg.update({(previous, current): dfg.get((previous, current), 0) + 1})
            if pprevious == current and previous != current:
                pre_short_loop.update({(previous, current): pre_short_loop.get((previous, current), 0) + 1})
            if previous == current:
                self_loop.update({current: self_loop.get(current, 0) + 1})

            pprevious, previous = previous, current
        current = '__FINAL__'
        dfg.update({(previous, current): dfg.get((previous, current), 0) + 1})
    
    # Now, we post-process the basic relations to determine Short-loop and (alpha) concurrency relations
    
    short_loop = set()
    pre_concurrency = set()
    concurrency = set()

    for a, b in dfg.keys():
        if (a not in self_loop.keys() and b not in self_loop.keys() and           # |a -> a| = 0 /\ |b -> b| = 0
            pre_short_loop.get((a, b),0) + pre_short_loop.get((b, a),0) != 0):    # |a <-> b| + |b <-> a| != 0
            short_loop.add((a,b))    
        if (a not in self_loop.keys() and b not in self_loop.keys() and           # |a -> a| = 0 /\ |b -> b| = 0
            dfg.get((a,b),0) > 0 and dfg.get((b,a), 0) > 0 and                    # |a -> b| > 0 /\ |b -> a| > 0
            pre_short_loop.get((a, b),0) + pre_short_loop.get((b, a),0) == 0):    # |a <-> b| + |b <-> a| = 0
            pre_concurrency.add((a,b))

    for a, b in pre_concurrency:
        conc_coeficient = abs((dfg[(a,b)] - dfg[(b,a)])/(dfg[(a,b)] + dfg[(b,a)]))
        if conc_coeficient < epsilon:
            concurrency.add((a, b))
            
    return (dfg, self_loop, short_loop, concurrency, pre_short_loop)

def filter_dfg(dfg):
    digraph = nx.DiGraph()
    digraph.add_weighted_edges_from([(a, b, dfg[(a,b)]) for a,b in dfg.keys()])

    rdigraph = nx.DiGraph()
    rdigraph.add_weighted_edges_from([(b, a, dfg[(a,b)]) for a,b in dfg.keys()])

    arborescence = nx.maximum_spanning_arborescence(digraph)
    rarborescence = nx.maximum_spanning_arborescence(rdigraph)

    fdfg = [(a, b) for a, b in arborescence.edges()]
    fdfg.extend([(b, a) for a, b in rarborescence.edges()])

    return set(fdfg)

def to_bpmn(process_graph):
    diagram = BpmnDiagramGraph()
    diagram.create_new_diagram_graph()
    proc = diagram.add_process_to_diagram()
    nodes = {}

    for vertex in set([node for pair in process_graph for node in pair]):
        if vertex == '__INITIAL__':
            node, _ = diagram.add_start_event_to_diagram(proc)
        elif vertex == '__FINAL__':
            node, _ = diagram.add_end_event_to_diagram(proc)
        elif vertex.startswith('XOR'):
            node, _ = diagram.add_exclusive_gateway_to_diagram(proc)
        elif vertex.startswith('AND'):
            node, _ = diagram.add_parallel_gateway_to_diagram(proc)
        elif vertex.startswith('OR'):
            node, _ = diagram.add_inclusive_gateway_to_diagram(proc)
        else:
            node, _ = diagram.add_task_to_diagram(proc, vertex)
        nodes[vertex] = node

    for src, tgt in process_graph:
        diagram.add_sequence_flow_to_diagram(proc, nodes[src], nodes[tgt])
        
    return diagram


def to_dot(dfg):
    edges = "".join(map(lambda pair: f'  {pair[0].replace(" ", "_")} -> {pair[1].replace(" ", "_")};\n' ,dfg))
    return f'digraph G {{\n{edges}}}'
                        
def to_wdot(dfg):
    edges = "".join(map(lambda pair: f'  {pair[0].replace(" ", "_")} -> {pair[1].replace(" ", "_")} [label="{dfg[pair]}"];\n' ,dfg))
    return f'digraph G {{\n{edges}}}'