from pm4py.objects.log.importer.xes import importer as xes_importer
from pm4pybpmn.objects.bpmn.util import bpmn_diagram_layouter
import sminer.sminer as sm
import sminer.sage_sminer as ssm

epsilon = 0.21
log = xes_importer.apply('logs/sepsis.xes.gz')
model = ssm.mine_model(log, epsilon)
diagram = sm.to_bpmn(model)
# bpmn_diagram_layouter.apply(diagram)
diagram.export_xml_file('output/', 'diagram.xml')
